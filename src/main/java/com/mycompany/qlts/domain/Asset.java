package com.mycompany.qlts.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Asset.
 */
@Entity
@Table(name = "QLTS_ASSETS")
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "serial")
    private String serial;

    @Column(name = "type")
    private String type;

    @Column(name = "use_time")
    private Integer useTime;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "department")
    private String department;

    @Column(name = "status")
    private String status;

    @Column(name = "setup_location")
    private String setupLocation;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "note")
    private String note;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Asset id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Asset name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public Asset code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSerial() {
        return this.serial;
    }

    public Asset serial(String serial) {
        this.setSerial(serial);
        return this;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getType() {
        return this.type;
    }

    public Asset type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUseTime() {
        return this.useTime;
    }

    public Asset useTime(Integer useTime) {
        this.setUseTime(useTime);
        return this;
    }

    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public Asset startDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDepartment() {
        return this.department;
    }

    public Asset department(String department) {
        this.setDepartment(department);
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStatus() {
        return this.status;
    }

    public Asset status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSetupLocation() {
        return this.setupLocation;
    }

    public Asset setupLocation(String setupLocation) {
        this.setSetupLocation(setupLocation);
        return this;
    }

    public void setSetupLocation(String setupLocation) {
        this.setupLocation = setupLocation;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public Asset quantity(Integer quantity) {
        this.setQuantity(quantity);
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return this.note;
    }

    public Asset note(String note) {
        this.setNote(note);
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asset)) {
            return false;
        }
        return id != null && id.equals(((Asset) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asset{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", serial='" + getSerial() + "'" +
            ", type='" + getType() + "'" +
            ", useTime='" + getUseTime() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", department='" + getDepartment() + "'" +
            ", status='" + getStatus() + "'" +
            ", setupLocation='" + getSetupLocation() + "'" +
            ", quantity=" + getQuantity() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
