// qr-code.component.ts
import { Component, Input, OnInit } from '@angular/core';
import QRCode from 'qrcode';

@Component({
  selector: 'app-qr-code',
  template: '<canvas #qrCanvas></canvas>',
})
export class QRCodeComponent implements OnInit {
  @Input() data: string = '';
  @Input() size: number = 200;

  ngOnInit(): void {
    this.generateQRCode();
  }

  private generateQRCode(): void {
    const canvas = document.createElement('canvas');
    QRCode.toCanvas(canvas, this.data, { width: this.size }, (error) => {
      if (error) {
        console.error(error);
      } else {
        const qrCanvas = document.querySelector('#qrCanvas');
        if (qrCanvas) {
          qrCanvas.appendChild(canvas);
        }
      }
    });
  }
}
