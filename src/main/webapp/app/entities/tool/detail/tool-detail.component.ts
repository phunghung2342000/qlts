import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { ITool } from '../tool.model';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import {NgxQrcodeElementTypes} from "ngx-qrcode2";
@Component({
  selector: 'jhi-tool-detail',
  templateUrl: './tool-detail.component.html',
})
export class ToolDetailComponent {
  tool: ITool | null = null;
  elementType: NgxQrcodeElementTypes = NgxQrcodeElementTypes.URL;

  constructor(protected activatedRoute: ActivatedRoute,protected activeModal: NgbActiveModal,  protected router: Router,private sanitizer: DomSanitizer) {}

  previousState(): void {
    this.router.navigate(['/tool']);
    this.activeModal.dismiss();
  }
  edit(tool: ITool): void {
    this.router.navigate(['/tool', tool.id, 'edit']);
    this.activeModal.dismiss();
  }

  downloadQRCode(): void {
    const qrCodeImage = document.querySelector('ngx-qrcode img') as HTMLImageElement;

    // Tạo URL an toàn cho hình ảnh
    const imageUrl = qrCodeImage.src;

    // Tạo một thẻ a để tải xuống
    const link = document.createElement('a');
    link.href = imageUrl;
    link.download = 'qrcode.png';

    // Thêm thẻ a vào DOM và kích hoạt sự kiện click để tải xuống
    document.body.appendChild(link);
    link.click();

    // Loại bỏ thẻ a sau khi tải xuống
    document.body.removeChild(link);
  }
}
