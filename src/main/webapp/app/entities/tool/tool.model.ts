import dayjs from 'dayjs/esm';

export interface ITool {
  id?: number;
  name?: string | null;
  code?: string | null;
  startDate?: dayjs.Dayjs | null;
  serial?: string | null;
  department?: string | null;
  owner?: string | null;
  unit?: string | null;
  quantity?: number | null;
  price?: number | null;
  amount?: number | null;
  status?: string | null;
  note?: string | null;
}

export class Tool implements ITool {
  constructor(
    public id?: number,
    public name?: string | null,
    public code?: string | null,
    public startDate?: dayjs.Dayjs | null,
    public serial?: string | null,
    public department?: string | null,
    public owner?: string | null,
    public unit?: string | null,
    public quantity?: number | null,
    public price?: number | null,
    public amount?: number | null,
    public status?: string | null,
    public note?: string | null
  ) {}
}

export function getToolIdentifier(tool: ITool): number | undefined {
  return tool.id;
}
