import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';

import { ITool } from '../tool.model';
import { ToolService } from '../service/tool.service';
import { ToolDeleteDialogComponent } from '../delete/tool-delete-dialog.component';
import {ToolDetailComponent} from "../detail/tool-detail.component";
import * as XLSX from 'xlsx';

@Component({
  selector: 'jhi-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.scss'],
})
export class ToolComponent implements OnInit {
  @ViewChild('content') content: TemplateRef<any> | undefined;
  tools?: ITool[];
  tool?: ITool[] | any;
  isLoading = false;
  page = 1;
  constructor(protected toolService: ToolService, protected modalService: NgbModal, ) {}

  loadAll(): void {
    this.isLoading = true;

    this.toolService.query().subscribe({
      next: (res: HttpResponse<ITool[]>) => {
        this.isLoading = false;
        this.tools = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.tool = [];
    this.loadAll();
  }

  newArr(lenght: number): any[] {
    if (lenght > 0) {
      return new Array(lenght);
    } else {
      return new Array(0);
    }
  }

  delete(tool: ITool): void {
    const modalRef = this.modalService.open(ToolDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tool = tool;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }

  import(): void {
    this.modalService.open(this.content,
        { size: 'lg', backdrop: 'static'}
    );
  }
  view(tool: ITool): void {
    const modalRef = this.modalService.open(ToolDetailComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.tool = tool;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
  closeModal(): void {
     this.modalService.dismissAll();
  }

  readExcel(event : any): void{
    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = (e: any) => {
      const workbook = XLSX.read(fileReader.result,{type:'binary'});
      const sheetNames = workbook.SheetNames;
      this.tool = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNames[0]]);
    };
  }

  importExcel(): void{
    if(this.tool.length > 0){
       this.toolService.createAll(this.tool).subscribe(
         data => {
           alert("thành công");
           this.closeModal();
           location.reload();
       }, error => {
         alert("có lỗi sảy ra");
       });
    }
  }
}
