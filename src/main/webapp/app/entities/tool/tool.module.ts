import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ToolComponent } from './list/tool.component';
import { ToolDetailComponent } from './detail/tool-detail.component';
import { ToolUpdateComponent } from './update/tool-update.component';
import { ToolDeleteDialogComponent } from './delete/tool-delete-dialog.component';
import { ToolRoutingModule } from './route/tool-routing.module';
import {NgxPaginationModule} from "ngx-pagination";
import {NgxQRCodeModule} from "ngx-qrcode2";

@NgModule({
    imports: [SharedModule, ToolRoutingModule, NgxPaginationModule, NgxQRCodeModule],
  declarations: [ToolComponent, ToolDetailComponent, ToolUpdateComponent, ToolDeleteDialogComponent],
  entryComponents: [ToolDeleteDialogComponent],
})
export class ToolModule {}
