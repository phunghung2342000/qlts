import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'tool',
        data: { pageTitle: 'Tools' },
        loadChildren: () => import('./tool/tool.module').then(m => m.ToolModule),
      },
      {
        path: 'asset',
        data: { pageTitle: 'Assets' },
        loadChildren: () => import('./asset/asset.module').then(m => m.AssetModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
