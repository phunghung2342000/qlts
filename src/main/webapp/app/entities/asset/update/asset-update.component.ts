import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IAsset, Asset } from '../asset.model';
import { AssetService } from '../service/asset.service';

@Component({
  selector: 'jhi-asset-update',
  templateUrl: './asset-update.component.html',
})
export class AssetUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    name: [],
    code: [],
    serial: [],
    type: [],
    useTime: [],
    startDate: [],
    department: [],
    status: [],
    setupLocation: [],
    quantity: [],
    note: [],
  });

  constructor(protected assetService: AssetService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ asset }) => {
      this.updateForm(asset);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const asset = this.createFromForm();
    if (asset.id !== undefined) {
      this.subscribeToSaveResponse(this.assetService.update(asset));
    } else {
      this.subscribeToSaveResponse(this.assetService.create(asset));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAsset>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(asset: IAsset): void {
    this.editForm.patchValue({
      id: asset.id,
      name: asset.name,
      code: asset.code,
      serial: asset.serial,
      type: asset.type,
      useTime: asset.useTime,
      startDate: asset.startDate,
      department: asset.department,
      status: asset.status,
      setupLocation: asset.setupLocation,
      quantity: asset.quantity,
      note: asset.note,
    });
  }

  protected createFromForm(): IAsset {
    return {
      ...new Asset(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
      serial: this.editForm.get(['serial'])!.value,
      type: this.editForm.get(['type'])!.value,
      useTime: this.editForm.get(['useTime'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      department: this.editForm.get(['department'])!.value,
      status: this.editForm.get(['status'])!.value,
      setupLocation: this.editForm.get(['setupLocation'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
      note: this.editForm.get(['note'])!.value,
    };
  }
}
