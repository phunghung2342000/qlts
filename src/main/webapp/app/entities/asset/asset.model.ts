import dayjs from 'dayjs/esm';

export interface IAsset {
  id?: number;
  name?: string | null;
  code?: string | null;
  serial?: string | null;
  type?: string | null;
  useTime?: number | null;
  startDate?: dayjs.Dayjs | null;
  department?: string | null;
  status?: string | null;
  setupLocation?: string | null;
  quantity?: number | null;
  note?: string | null;
}

export class Asset implements IAsset {
  constructor(
    public id?: number,
    public name?: string | null,
    public code?: string | null,
    public serial?: string | null,
    public type?: string | null,
    public useTime?: number | null,
    public startDate?: dayjs.Dayjs | null,
    public department?: string | null,
    public status?: string | null,
    public setupLocation?: string | null,
    public quantity?: number | null,
    public note?: string | null
  ) {}
}

export function getAssetIdentifier(asset: IAsset): number | undefined {
  return asset.id;
}
