import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { IAsset } from '../asset.model';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ITool} from "../../tool/tool.model";
import {NgxQrcodeElementTypes} from "ngx-qrcode2";

@Component({
  selector: 'jhi-asset-detail',
  templateUrl: './asset-detail.component.html',
})
export class AssetDetailComponent{
  asset: IAsset | null = null;
  elementType: NgxQrcodeElementTypes = NgxQrcodeElementTypes.URL;
  constructor(protected activatedRoute: ActivatedRoute,protected activeModal: NgbActiveModal,protected router: Router) {}


  previousState(): void {
    this.router.navigate(['/asset']);
    this.activeModal.dismiss();
  }
  edit(tool: ITool): void {
    this.router.navigate(['/asset', tool.id, 'edit']);
    this.activeModal.dismiss();
  }

  downloadQRCode(): void {
    const qrCodeImage = document.querySelector('ngx-qrcode img') as HTMLImageElement;

    // Tạo URL an toàn cho hình ảnh
    const imageUrl = qrCodeImage.src;

    // Tạo một thẻ a để tải xuống
    const link = document.createElement('a');
    link.href = imageUrl;
    link.download = 'qrcode.png';

    // Thêm thẻ a vào DOM và kích hoạt sự kiện click để tải xuống
    document.body.appendChild(link);
    link.click();

    // Loại bỏ thẻ a sau khi tải xuống
    document.body.removeChild(link);
  }
}
