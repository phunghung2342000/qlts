import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAsset, getAssetIdentifier } from '../asset.model';

export type EntityResponseType = HttpResponse<IAsset>;
export type EntityArrayResponseType = HttpResponse<IAsset[]>;

@Injectable({ providedIn: 'root' })
export class AssetService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/assets');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(asset: IAsset): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(asset);
    return this.http
      .post<IAsset>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(asset: IAsset): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(asset);
    return this.http
      .put<IAsset>(`${this.resourceUrl}/${getAssetIdentifier(asset) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(asset: IAsset): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(asset);
    return this.http
      .patch<IAsset>(`${this.resourceUrl}/${getAssetIdentifier(asset) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAsset>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAsset[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAssetToCollectionIfMissing(assetCollection: IAsset[], ...assetsToCheck: (IAsset | null | undefined)[]): IAsset[] {
    const assets: IAsset[] = assetsToCheck.filter(isPresent);
    if (assets.length > 0) {
      const assetCollectionIdentifiers = assetCollection.map(assetItem => getAssetIdentifier(assetItem)!);
      const assetsToAdd = assets.filter(assetItem => {
        const assetIdentifier = getAssetIdentifier(assetItem);
        if (assetIdentifier == null || assetCollectionIdentifiers.includes(assetIdentifier)) {
          return false;
        }
        assetCollectionIdentifiers.push(assetIdentifier);
        return true;
      });
      return [...assetsToAdd, ...assetCollection];
    }
    return assetCollection;
  }

  protected convertDateFromClient(asset: IAsset): IAsset {
    return Object.assign({}, asset, {
      startDate: asset.startDate?.isValid() ? asset.startDate.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? dayjs(res.body.startDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((asset: IAsset) => {
        asset.startDate = asset.startDate ? dayjs(asset.startDate) : undefined;
      });
    }
    return res;
  }
}
