import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AssetComponent } from './list/asset.component';
import { AssetDetailComponent } from './detail/asset-detail.component';
import { AssetUpdateComponent } from './update/asset-update.component';
import { AssetDeleteDialogComponent } from './delete/asset-delete-dialog.component';
import { AssetRoutingModule } from './route/asset-routing.module';
import {NgxQRCodeModule} from "ngx-qrcode2";

@NgModule({
    imports: [SharedModule, AssetRoutingModule, NgxQRCodeModule],
  declarations: [AssetComponent, AssetDetailComponent, AssetUpdateComponent, AssetDeleteDialogComponent],
  entryComponents: [AssetDeleteDialogComponent],
})
export class AssetModule {}
